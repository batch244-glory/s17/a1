console.log("hello world");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	
	let promptName = prompt("What is your name?");
	let promptAge = prompt("How old are you?");
	let promptCity = prompt("Where do you live?");

		function information(){
			console.log("Hello, " + promptName);
			console.log("You are " + promptAge);
			console.log("You live in " + promptCity);
		}

		information();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favBands (){
		console.log("1. Incubus1");
		console.log("2. Incubus2");
		console.log("3. Incubus3");
		console.log("4. Incubus4");
		console.log("5. Incubus5");
	}

	favBands ();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	let rt
	function favMovies (){
		let movie1 = "The Lord of the Rings: The Fellowship of the Ring"
		console.log("1. " + movie1);
		console.log("Rotten Tomatoes Rating: 91%");

		let movie2 = "The Lord of the Rings: The Two Towers";
		console.log("2. " + movie2);
		console.log("Rotten Tomatoes Rating: 95%");

		let movie3 = "The Lord of the Rings: The Return of the King"
		console.log("3. " + movie3);
		console.log("Rotten Tomatoes Rating: 93%");

		let movie4 = "Troy"
		console.log("4. " + movie4);
		console.log("Rotten Tomatoes Rating: 53%");

		let movie5 = "The Matrix"
		console.log("5. " + movie5);
		console.log("Rotten Tomatoes Rating: 88%");
	}

	favMovies();




/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	//printUsers();
	function printFriends() /*= function printUsers()*/{
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");
		console.log("You are friends with:")
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	};
	printFriends();

	/*console.log(friend1);
	console.log(friend2);*/